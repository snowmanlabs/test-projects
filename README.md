# BOLDTECH TESTE #

To start, run a local server and build the application, follow the steps below:

### Installing NodeJS ###

* Download and install: [NodeJS](https://nodejs.org/en/download/).

### Installing Yeoman ###

* On terminal, go the project folter and run `npm install -g yo gulp bower generator-gulp-angular`. This will install Yeoman and all its dependencies, including the Angular Generator (gulp).

### Running a local server ###

* On terminal, go the project folter and run `gulp serve`. If you would like to simulate a local server for a built version, you can then run `gulp serve:dist`.

### Building the project ###

* On terminal, go the project folter and run `gulp build`.