(function () {
	'use strict';

	angular.module('boldAngularTest').controller('MainController', ['MainService', '$timeout', MainController]);

	/**
     * MainController to deal with the Server view
     * @param {Object} MainService [The MainService that deals with the Servers availability]
     */
	function MainController(MainService, $timeout) {

		// setting a local variable
		var self = this;

		// initializing busy variables
		self.availableServersBusy = true;
		self.lessPriorityOnlineServerBusy = true;

		// initializing the services variables
		self.lessPriorityOnlineServer = null;
		self.availableServers = null;

		// passing the methods to the view
		self.getLessPriorityOnlineServer = getLessPriorityOnlineServer;
		self.toggleStatus = toggleStatus;
		self.toggleTimeout = toggleTimeout;

		// getting the list of available servers
		MainService.getServers().then(function (servers) {
			self.availableServersBusy = false;
			self.availableServers = servers;
		}, function () {
			self.availableServersBusy = false;
			self.availableServers = null;
		});

		// getting the less priority online server
		MainService.findServer().then(function (server) {
			self.lessPriorityOnlineServerBusy = false;
			self.lessPriorityOnlineServer = server;
		}, function () {
			self.lessPriorityOnlineServerBusy = false;
			self.lessPriorityOnlineServer = null;
		});

		/**
     * function to get the less priority online server, since it has been changed
     * @return {Void}
     */
		function getLessPriorityOnlineServer() {
			MainService.getLessPriorityOnlineServer().then(function (server) {
				self.lessPriorityOnlineServer = server;
			}, function () {
				self.lessPriorityOnlineServer = null;
			});
		}

		/**
     * Function to toggle the server if is online or not
     * @param  {Object} server [a Server object model]
     * @return {Void}
     */
		function toggleStatus(server) {
			server.hasTimeout = false;
			server.isOnline = !server.isOnline;
			getLessPriorityOnlineServer();
		}

		/**
     * Function to toggle the server if it has timeout or not
     * @param  {Object} server [a Server object model]
     * @return {Void}
     */
		function toggleTimeout(server) {
			server.hasTimeout = !server.hasTimeout;
			server.isOnline = false;
			getLessPriorityOnlineServer();
		}

	}
})();
