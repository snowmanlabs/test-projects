(function () {
	'use strict';

	// registering a Angular Service into the application
	angular.module('boldAngularTest').service('MainService', ['$http', '$q', MainService]);

	/**
   * MainService to deal with the Servers availability
   * @param {Object} $http [The $http core AngularJS service]
   * @param {Object} $q    [The $q helps run functions asynchronously]
   */
	function MainService($http, $q) {

		// setting a local variable
		var self = this;

		// setting the available servers
		var servers = [
			{
				"url": "http://doesNotExist.boldtech.co",
				"priority": 1
			}, {
				"url": "http://boldtech.co",
				"priority": 7
			}, {
				"url": "http://offline.boldtech.co",
				"priority": 2
			}, {
				"url": "http://google.com",
				"priority": 4
			}
		];

		/**
		 * Functiont o return the list of servers available
		 * @return {Promise} [Array promise within servers available]
		 */
		function getServers() {

			// returning a promise
			return $q(function (resolve) {
				resolve(servers);
			});

		}

		/**
		 * Function to process and return the less priority online server model
		 * @return {Promise} [a Server model]
		 */
		function getLessPriorityOnlineServer() {

			// create a promise
			var deferred = $q.defer();
			var lessPriorityOnlineServer;

			// filtering all the online servers
			lessPriorityOnlineServer = servers.filter(function (server) {
				return server.isOnline;
			});

			// getting the online server that has the lowest priority number
			lessPriorityOnlineServer = lessPriorityOnlineServer.filter(function (item) {
				return item.priority === Math.min.apply(Math, lessPriorityOnlineServer.map(function (server) {
					return server.priority;
				}));
			})[0];

			// resolving if there is one server available
			if (lessPriorityOnlineServer) {
				deferred.resolve(lessPriorityOnlineServer// rejecting if there is NO server available
				);
			} else {
				deferred.reject(null);
			}

			// return the promise itself
			return deferred.promise;

		}

		/**
     * Function to resolves with the online server that has the lowest
     * priority number
     * @return {Promise} [online server that has the lowest]
     */
		function findServer() {

			// create a promise
			var deferred = $q.defer();

			// set a commun callbac
			var callback = function (server, index) {

				var i;

				// checking if all servers has already done the ajax call
				for (i in servers) {
					if (servers[i].hasFinished === false) {
						return false;
					}
				}

				getLessPriorityOnlineServer().then(function (lessPriorityOnlineServer) {
					deferred.resolve(lessPriorityOnlineServer);
				}, function () {
					deferred.reject(null);
				});

			};

			// looping the servers list
			angular.forEach(servers, function (server, index) {

				// setting a initial state for the server
				server.hasFinished = false;

				// test the call
				$http.get(server.url, {timeout: 5000}).then(function (response) {

					// set the state to true, so we know that it has finished
					server.hasFinished = true;

					// checking if the server status is what we need
					server.isOnline = (response.status >= 200 && response.status < 300);

					// has timeout?
					server.hasTimeout = false;

					// call the validation callback
					callback();

				}, function (error) {

					// set the state to true, so we know that it has finished
					server.hasFinished = true;

					// setting the server as false, since we get a error, this server
					// is OFF service
					server.isOnline = false;

					// has timeout?
					if (error.status === 408) {
						server.hasTimeout = true;
					} else {
						server.hasTimeout = false;
					}

					// call the validation callback
					callback();

				});

			});

			// return the promise itself
			return deferred.promise;

		}

		// registering the public methods
		self.getServers = getServers;
		self.findServer = findServer;
		self.getLessPriorityOnlineServer = getLessPriorityOnlineServer;

	}

})();
