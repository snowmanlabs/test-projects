(function () {
	'use strict';

	angular.module('boldAngularTest', ['ui.router', 'ui.bootstrap', 'toastr']).config(['$stateProvider', '$urlRouterProvider', '$logProvider', '$httpProvider', bootstrap]);

	function bootstrap($stateProvider, $urlRouterProvider, $logProvider, $httpProvider) {

		// ENABLE LOG
		$logProvider.debugEnabled(true);

		// UI-ROUTER
		$stateProvider.state('main', {
			url: '/',
			templateUrl: 'app/components/main/main.html',
			controller: 'MainController',
			controllerAs: 'main'
		});

		$urlRouterProvider.otherwise('/');

		//$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		//$httpProvider.defaults.headers.common['Content-type'] = 'application/x-www-form-urlencoded; charset=utf-8';
		//$httpProvider.defaults.headers.post["Content-Type"] = "text/plain";

	}

})();
